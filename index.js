const http = require('http');
const createGreeting = require('./greeter.js');

const port = 3000;

const server = http.createServer((req, res) => {
  const greeting = createGreeting('Ashot')
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end(greeting);
});

server.listen(port, () => {
  console.log(`HTTP server running on TCP port ${port}`);
});
