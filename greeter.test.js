const greeter = require('./greeter.js')

test('Should greet Samvel', () =>{
    expect(greeter('Samvel')).toBe('Hello Samvel')
})